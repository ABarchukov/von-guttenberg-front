import { TimelineMax, TweenMax } from "gsap/TweenMax";
// import TweenMax from "gsap/TweenMaxBase";
import { throttle, debounce } from 'throttle-debounce';
import { DOM, CLASSES } from './helpers/_consts';
import isTouch from './helpers/_detectTouch';

const btnClass = 'js-pagination-btn';
const imgClass = 'js-hero-slider-img';
const textBlockClass = 'js-hero-slider-text';
const titleClass = 'js-hero-slider-title'

class HeroSlider {
  constructor(options) {
    this.$el = options.slider;
    this.startSlide = options.startSlide;
    this.CHANGE_DURATION = options.CHANGE_DURATION;
    this.SLIDE_SHOW_DURATION = options.SLIDE_SHOW_DURATION;
    this.$imgs = this.$el.find(`.${imgClass}`);
    this.$counterWrapper = this.$el.find('.js-hero-slider-counter');
    this.$textWrapper = this.$el.find('.js-hero-slider-text-wrapper');
    this.$textBlocks = this.$el.find(`.${textBlockClass}`);
    this.$titles = this.$el.find(`.${titleClass}`);
    this.$paginationWrapper = this.$el.find('.js-hero-slider-pagination');
    this.$textLine = this.$el.find('.js-hero-slider-line');
    this.canChangeSlide = true;
    this.btnClicked = false;
    this.state = 0; 
    this.activeIndex = 0;
    this.prevIndex = 0;
    this.init();
  }

  init() {
    this.slidesCount = this.$imgs.length;
    this.startSlide = this.startSlide > this.slidesCount ? 0 : this.startSlide;
    this.activeIndex = this.startSlide;
    this.preparePagination();
    this.prepareCounter();
    this.prepareAttrs(this.$imgs);
    this.prepareAttrs(this.$textBlocks);
    this.prepareAttrs(this.$titles);
    // this.setTextHeight();
    this.setEvents();
    this.calcDefaultLinePos();
    // this.setPosLine();
    this.resizeHandle();
    this.start();
  }

  start() {
    this.paginationAnim();
  }

  paginationAnim() {
    if (this.canChangeSlide === false) return;
    this.btnGrowAnim();
  }

  // click btn and helpers
  btnClickHandle(e) {
    e.preventDefault();

    this.clickIndex = +$(e.currentTarget).attr('data-index');

    if(this.canChangeSlide === false && this.state === 'changing'){
      if (this.btnClicked || this.btnOneClick) return;
      this.btnOneClick = true;
      this.btnClicked = true;
      if(this.clickIndex === this.prevIndex){
        this.btnClickActiveDecreases();
      }else{
        this.btnClickOtherDecreases();
      }
      return;
    }
    if (this.canChangeSlide === false) return;
    this.canChangeSlide = false;

    if(this.clickIndex === this.activeIndex){
      if (this.btnClicked) return;
      this.btnClickOnGrowing();
      return;
    }

    this.btnHelperFunc();
    this.btnGrowAnim();
    this.changeSlide();
  }

  btnClickActiveDecreases(){
    this.changeTl.stop();
    this.activeIndex = this.clickIndex;

    this.btnDecreaseReverse();
    this.reverseActiveSlideChanging();
  }

  btnClickOtherDecreases(){
    this.activeIndex = this.clickIndex;
    this.changeTl.stop();
    this.prepareSlideChangingAnim();
    this.btnGrowTl.duration(this.CHANGE_DURATION);
    this.btnGrowAnim(this.CHANGE_DURATION);
    this.changeTl.play();
    // if(this.isAnimateTextHeight) this.animateTextHeight();
  }

  btnClickOnGrowing(){
    this.btnGrowTl.stop();
    this.btnGrowTl.duration(this.CHANGE_DURATION);
    this.btnGrowTl.eventCallback("onComplete", () => {this.canChangeSlide = true;})
    this.btnHelperFunc();
    this.btnGrowTl.play();
  }
  
  btnHelperFunc(){
    this.activeIndex = this.clickIndex; 
    this.$activeBtn = this.$newBtn;
    this.SLIDE_SHOW_DURATION = this.CHANGE_DURATION;
    this.btnClicked = true;
  }

  btnDecreaseReverse(){
    const $btnToAmin = this.$activeBtn.find('.pag-btn__progress');
    this.btnDecreaseTl.stop();

    this.btnDecreaseTl = new TimelineMax({paused: true});
    this.btnDecreaseTl
      .to($btnToAmin, this.CHANGE_DURATION, {x: 0} , 0)

    this.btnDecreaseTl.play();
  }

  // change slide 
  btnGrowAnim(dur){
    this.btnGrowTl = new TimelineMax({
        paused: true,
        onComplete: this.btnCompleteAnimHandle.bind(this) 
      })

    this.$newBtn = $(`.${btnClass}[data-index="${this.activeIndex}"]`);
    const $btnToAmin = this.$newBtn.find('.pag-btn__progress');
    const duration = dur || this.SLIDE_SHOW_DURATION;
    this.btnGrowTl
      .fromTo($btnToAmin, duration, {opacity: 1, x: 0, scaleX: 0}, {opacity: 1, x: 0, scaleX: 1}, 0)
    this.btnGrowTl.play();
  }

  btnDecrease(){
    const $btnToAmin = this.$activeBtn.find('.pag-btn__progress');
    const moveXTo = $btnToAmin.width();

    this.btnDecreaseTl = new TimelineMax({paused: true});
    this.btnDecreaseTl
      .fromTo($btnToAmin, this.CHANGE_DURATION, {x: 0}, {x: moveXTo + 1  }, 0)
      .to($btnToAmin, 0, {x: 100} , '+=0')

    this.btnDecreaseTl.play();
  }

  changeSlideIndex() {
    this.prevIndex = this.activeIndex;
    this.activeIndex = this.activeIndex + 1;

    if (this.activeIndex >= this.slidesCount) {
      this.activeIndex = 0;
    }
  }

  changeSlide() {
    this.prepareSlideChangingAnim();
    this.btnDecrease();
    this.changeTl.play();
    this.calcLinePos();
    this.animateLine();

    // if(this.isAnimateTextHeight) this.animateTextHeight();

  }
  
  prepareSlideChangingAnim(){
    const showDuration = this.CHANGE_DURATION - this.CHANGE_DURATION / 4;

    this.canChangeSlide = false;
    this.state = 'changing'
    if(this.btnClicked){
      this.changeTl = new TimelineMax({ paused: true, onComplete: () => {this.canChangeSlide = true} })
    }else{
      this.changeTl = new TimelineMax({
        paused: true,
        // onStart: this.showVisibleSlideHandle.bind(this),
        onComplete: this.completeSlideChangingHandle.bind(this) 
      })
    }
    this.changeTl = new TimelineMax({
      paused: true,
      // onStart: this.showVisibleSlideHandle.bind(this),
      onComplete: this.completeSlideChangingHandle.bind(this) 
    })
    
    this.$newCounterNumber = this.$counterWrapper.find(`[data-index=${this.activeIndex}]`);
    this.$newImage = this.$imgs.filter(`[data-index=${this.activeIndex}]`);
    this.$newTextBlock = this.$textBlocks.filter(`[data-index=${this.activeIndex}]`);

    this.changeTl
      .to(this.$counters, this.CHANGE_DURATION, { opacity: 0 }, 0)
      .to(this.$imgs, this.CHANGE_DURATION, { opacity: 0 }, 0)
      .to(this.$textBlocks, this.CHANGE_DURATION, { opacity: 0 }, 0)
      .to(this.$newCounterNumber, this.CHANGE_DURATION, { opacity: 1 }, `-=${showDuration}`)
      .to(this.$newImage, this.CHANGE_DURATION, { opacity: 1 }, `-=${showDuration}`)
      .to(this.$newTextBlock, this.CHANGE_DURATION, { opacity: 1, onStart: () => {this.$newTextBlock.addClass(CLASSES.visible)} }, `-=${showDuration}`)
  }

  reverseActiveSlideChanging(){
    this.changeTl = new TimelineMax({ paused: true, onComplete: () => {this.canChangeSlide = true} });
    const $activeCounter = this.$counters.eq(this.prevIndex);
    const $activeImg = this.$imgs.eq(this.prevIndex);

    this.changeTl
      .to(this.$counters, this.CHANGE_DURATION, { opacity: 0 }, 0)
      .to(this.$imgs, this.CHANGE_DURATION, { opacity: 0 }, 0)
      .to(this.$textBlocks, this.CHANGE_DURATION, { opacity: 0, onStart: () => {this.$textBlocks.removeClass(CLASSES.visible)} }, 0)
      .to($activeCounter , this.CHANGE_DURATION, { opacity: 1 }, 0)
      .to($activeImg, this.CHANGE_DURATION, { opacity: 1 }, 0)
      .to(this.$activeTextBlock, this.CHANGE_DURATION, { opacity: 1, onStart: () => {this.$activeTextBlock.addClass(CLASSES.visible)} }, 0);

    this.calcLinePos();
    this.changeTl.play();
    this.animateLine();
    // if(this.isAnimateTextHeight) this.animateTextHeight();
  }

  // showVisibleSlideHandle(){
  //   this.$activeTextBlock.addClass(CLASSES.visible);
  // }

  btnCompleteAnimHandle() {
    if(this.btnClicked) return;
    this.$activeBtn = this.$newBtn;
    this.changeSlideIndex();
    this.changeSlide();
  }

  completeSlideChangingHandle() {
    // this.$activeCounterNumber = this.$newCounterNumber;
    this.$activeTextBlock.removeClass(CLASSES.visible);
    this.$activeImg = this.$newImage;
    this.$activeTextBlock = this.$newTextBlock;
    // this.$activeTextBlock.addClass(CLASSES.visible);

    this.canChangeSlide = true;
    this.state = '';
    if(this.btnClicked === false) this.paginationAnim();
  }

  //  prepare DOM
  preparePagination() {
    for (let index = 0; index < this.slidesCount; index++) {
      const $btn = $('<div>');

      $btn.addClass(`pag-btn ${btnClass}`);
      $btn.attr('data-index', index);

      const $btnInner = $('<div>');
      $btnInner.addClass('pag-btn__inner');
      $btnInner.append(`<div class="pag-btn__line"></div><div class="pag-btn__progress"></div>`);
      $btn.append($btnInner);
      this.$paginationWrapper.append($btn);
    }
    this.$paginBtns = $(`.${btnClass}`);
  }

  prepareAttrs(elems) {
    elems.each((i, el) => {
      const $elem = $(el);
      $elem.attr('data-index', i);

      if (i === this.activeIndex) {
        if ($elem.hasClass(imgClass)) {
          this.$activeImg = $elem;
        } else if ($elem.hasClass(textBlockClass)) {
          this.$activeTextBlock = $elem;
        }
        $elem.addClass(CLASSES.visible);
      };
    });
  }

  prepareCounter() {
    for (let i = 0; i < this.slidesCount; i++) {
      const $number = $('<div>');
      $number.addClass('title title_counter hero-slider__counter-number');

      if (i === this.activeIndex) {
        $number.addClass(CLASSES.visible);
        this.$activeCounterNumber = $number;
      };

      $number.attr('data-index', i);
      $number.html(i + 1)
      this.$counterWrapper.append($number);
      this.$counters = this.$counterWrapper.find('div');
    }
  }

  calcMaxTextHeight(){
    this.textHeight = 0;

    this.$textBlocks.each((i, el)=>{
      const height = $(el).innerHeight();
      this.textHeight = height > this.textHeight ? height: this.textHeight
    })
  }

  calcActiveTextHeight(){
    this.textHeight = this.$textBlocks.eq(this.activeIndex).innerHeight();
  }

  setTextHeight(){
    this.calcMaxTextHeight();
    this.$textWrapper.css('height', `${this.textHeight}px`);
  }

  animateTextHeight(){
    this.calcActiveTextHeight();
    TweenMax
      .to(this.$textWrapper, this.CHANGE_DURATION, {css: {height: `${this.textHeight}px`}})

  }

  //  line
  calcDefaultLinePos(){
    this.textLinePos = $(`.${titleClass}[data-index="${this.activeIndex}"]`).innerHeight()/2 - this.$textLine.innerHeight()/2;
  }

  calcLinePos(){
    this.textLinePos = $(`.${titleClass}[data-index="${this.activeIndex}"]`).innerHeight()/2 - this.$textLine.innerHeight()/2;
  }

  animateLine(){
    TweenMax.to(this.$textLine, this.CHANGE_DURATION, {x: 0, y: this.textLinePos})
  }

  setPosLine(){
    TweenMax.to(this.$textLine, 0, {x: 0, y: this.textLinePos})
  }
  
  // events

  setEvents() {
    this.$paginationWrapper.on('click', `.${btnClass}`, this.btnClickHandle.bind(this));
    const self = this;
    let resizeFunc;

    if(isTouch()){
      resizeFunc = debounce(300, () => {self.resizeHandle()});
    }else{
      resizeFunc = throttle(300, () => {self.resizeHandle()});
    }
    
    DOM.$win.resize(resizeFunc)
  }

  resizeHandle(){

    this.calcLinePos();
    this.setPosLine();
    this.setTextHeight();
    // if(DOM.$win.innerWidth() <= BREAKPOINTS.mobile){
    //   // this.isAnimateTextHeight = true;
    //   // this.calcActiveTextHeight();
    //   // this.animateTextHeight();
    //   this.setTextHeight();
    // }else{
    //   this.setTextHeight();
    //   // this.isAnimateTextHeight = false;
    //   // this.setTextHeight();
    // }
  }

  // end class
}

DOM.$win.ready(() => {
  const $slider = $('.js-hero-slider');
  if (!$slider[0]) return;
  if ($slider.hasClass('is-one-slide')) return;

  const options = {
    slider: $slider,
    startSlide: 0,
    CHANGE_DURATION: 0.5,
    SLIDE_SHOW_DURATION: 3.5
  };
  const sliderAttrs = [
    'data-start-slide',
    'data-slide-change-duration',
    'data-slide-show-duration'
  ];

  sliderAttrs.forEach(el => {
    const attr = $slider.attr(el)

    if(attr === undefined) return;

    let data;
    if(el === 'data-start-slide'){
      data = parseInt(attr, 10);
    }else{
      data = parseFloat(attr);
    };

    if(typeof(data) !== 'number') return; 

    if(el === 'data-start-slide'){
      options.startSlide = data;
    }else if(el === 'data-slide-change-duration'){
      options.CHANGE_DURATION = data;
    }else if(el === 'data-slide-show-duration'){
      options.SLIDE_SHOW_DURATION = data;
    }
  });

  /* eslint-disable-next-line */ 
  const herSlider = new HeroSlider(options);
});
