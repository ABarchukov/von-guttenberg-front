import { DOM, GLOBAL } from './_consts';

function detectBrowser() {
  if(GLOBAL.browser === 'firefox') {
    DOM.$body.addClass('is-firefox');
  }else if (GLOBAL.browser === 'ie') {
    DOM.$body.addClass('is-ie');
  }else if (GLOBAL.browser === 'edge') {
    DOM.$body.addClass('is-edge');
  }else if (GLOBAL.browser === 'chrome') {
    DOM.$body.addClass('is-chrome');
  }
}

detectBrowser();