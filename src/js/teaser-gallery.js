import Swiper from 'swiper/dist/js/swiper';

import { DOM } from './helpers/_consts';

const wrapperClass = 'js-teaser-gallery-wrapper'

class SimpleGallery {
  constructor(options) {
    this.$container = options.el;
    this.$pagination = this.$container.find('.js-teaser-gallery-pagination');
    this.$slider = this.$container.find('.js-teaser-gallery-slider')
    this.init();
  }


  init(){
    const self = this;
    this.slider = new Swiper(self.$slider, {
      slidesPerView: 1,
      loop: true,
      autoplay: true,
      // spaceBetween: 20,
      // init: false,
      simulateTouch: true,
      // on: {
      //   init: this.resizeHandle.bind(this) ,
      //   resize: debounce(300, () => {self.resizeHandle()})
      // },
      pagination: {
        el: this.$pagination,
        clickable: true,
        renderBullet (index, className) {
          const btn = `
          <div class="pag-btn pag-btn_custom ${className} " data-index="${index}">
            <div class="pag-btn__inner">
            <div class="pag-btn__line"></div>
            <div class="pag-btn__progress"></div>
            </div>
          </div>`

          return btn;
        }
      },
      // breakpoints: {

      //   959: {
      //     slidesPerView: 2,
      //     slidesPerGroup: 2
      //   },
      //   719: {
      //     slidesPerView: 1,
      //     slidesPerGroup: 1
      //   }
      // }
    });

  }

  // end class
}

DOM.$win.ready(() => {
  const $sliders = $(`.${wrapperClass}`);

  $sliders.each((i, el) => {
    const $el = $(el);
      /* eslint-disable-next-line */
    const herSlider = new SimpleGallery({el: $el});
  })
});
