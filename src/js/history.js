// import Swiper from 'swiper';

// import PerfectScrollbar from 'perfect-scrollbar';
import Scroller from './modules/_scroller';
import { DOM } from './helpers/_consts';






DOM.$win.ready(() => {
  const wrapperClass = '.js-history-scroller-wrapper'



  /* eslint-disable-next-line */
  $(wrapperClass).each((i, el) => {
    const $el = $(el);


    const options = {
      wrapper: $el,
      sliderClass: 'js-scroller-slider'
    }
    
    /* eslint-disable-next-line */ 
    const scroller = new Scroller(options)
  })


});
