import mapboxgl from 'mapbox-gl';
import  './helpers/_easings';

import { DOM } from './helpers/_consts';

import Accordion from './modules/_accordion';

DOM.$win.ready(() => {
  const $accordion = $('.js-locations-accordion');
  const options = {
    el: $accordion,
    changeDuration: 0.4
  };
  $accordion.each((i, el) => {
    const maps = [];
    const $el = $(el)
    const $maps = $el.find('.js-location-map');
    options.el = $el;
       /* eslint-disable-next-line */

    $maps.each((inder, elem) => {
      const key = window.MapsSettings.mapApiKey;
      const styles = window.MapsSettings.mapApiUrl;
      // const key = 'pk.eyJ1Ijoia29zdGVua29zIiwiYSI6ImNsMDg4Z2s1bzAwMWIzY3F0MGRscWQycnEifQ.p_9wzJTpFk4NweFok7Gi5Q';
      // const styles = 'mapbox://styles/kostenkos/cm3iniaua00f601s9gworhraz';

      mapboxgl.accessToken = key;
      const lng = +elem.getAttribute('data-lng');
      const lat = +elem.getAttribute('data-lat');
      const zoom = +elem.getAttribute('data-zoom');
      const	map = new mapboxgl.Map({
        container: elem,
        // style: 'mapbox://styles/kostenkos/cm3ih8qsd00j901seaku0g9ci', // style URL
        // style: 'mapbox://styles/kostenkos/cm3g9dh5f003r01r81u2kdp1v', // style URL
        style: styles, // style URL
        // style: 'mapbox://styles/jonassembly/cly8pbftn00hy01pmadqw3njx', // style URL
        center: [lng, lat],
        zoom: zoom || 14,

      });

      map.scrollZoom.disable();
      map.addControl(new mapboxgl.NavigationControl());

      const Mark = document.createElement('div');
      Mark.className = 'my-icon';
      const marker = new mapboxgl.Marker({
        color: '#FF0000'
      })
        .setLngLat({
          // lng: -3.235,
          // lat: 53.340
          lng,
          lat
        })
        .addTo(map);
      maps.push(map);
    });

    options.onStart = (elem) => {
      maps.forEach((map) => {
         /* eslint-disable-next-line */
        if (map._container.closest('.accordion.is-open')) {
          map.resize();
        }
      });
    }
    const $accord = new Accordion(options)
  })
});



// const initMap = (el, block) => {
// 	const markers = [];
// 	let map = null;
// 	let ticker = null;
// 	let currentId = 0;

// 	const imgLinks = block.querySelectorAll('.js-grid-map-info-link');
// 	const listBtns = block.querySelectorAll('.js-grid-map-info-list-btn');


// 	// my
// 	// mapboxgl.accessToken = 'pk.eyJ1Ijoia29zdGVua29zIiwiYSI6ImNsMDg4Z2s1bzAwMWIzY3F0MGRscWQycnEifQ.p_9wzJTpFk4NweFok7Gi5Q';

// 	// client
// 	mapboxgl.accessToken = 'pk.eyJ1Ijoiam9uYXNzZW1ibHkiLCJhIjoiY2x5NHcxNDRrMDgwMTJpczdyYjN5M2NzZSJ9.TEUSI8ATNT8rHndOI-PvLA';

// 	let zoom = IS_TABLET_SM ? 5.1 : 5.5;
// 	zoom = IS_PHONE ? 5 : zoom;

// 	map = new mapboxgl.Map({
// 		container: el,
// 		// style: 'mapbox://styles/kostenkos/cly5x01ja00d301qvfnxcegf0', // style URL
// 		style: 'mapbox://styles/jonassembly/cly8pbftn00hy01pmadqw3njx', // style URL
// 		center: [-3.235, 53.340],
// 		zoom: zoom,

// 	});
// 	map.scrollZoom.disable();
// 	if (!IS_PHONE && !IS_TABLET_SM) map.addControl(new mapboxgl.NavigationControl());

// 	markersData.forEach(data => {
// 		const el = document.createElement('div');
// 		const id = +data.id;
// 		el.className = 'my-icon';
// 		el.setAttribute('data-id', id);

// 		const marker = new mapboxgl.Marker(el)
// 			.setLngLat(data.coordinates)
// 			.addTo(map);

// 		marker.getElement().addEventListener('click', () => {
// 			changeMapItem(id);
// 		});

// 		markers.push(marker);
// 	});

// 	listBtns.forEach(btn => {
// 		btn.addEventListener('click', () => {
// 			const id = +btn.getAttribute('data-id');
// 			currentId = id;
// 			changeMapItem(currentId);
// 		});
// 	});

// };
