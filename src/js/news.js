import { DOM } from './helpers/_consts';

DOM.$win.ready(() => {
  const $sections = $('.js-news-section-wrapper');
  $sections.each((index, section) => {
    const $moreBtn = $(section).find('.js-news-more-btn');
    const $newsHiddenItems = $(section).find('.js-news-item.is-hidden');
    $moreBtn.on('click', (e) => {
      e.preventDefault();
      $newsHiddenItems.removeClass('is-hidden');
      $moreBtn.remove();
    });
  });
});
