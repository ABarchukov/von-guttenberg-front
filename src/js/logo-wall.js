import  './helpers/_easings';

import { DOM } from './helpers/_consts';

import Accordion from './modules/_accordion';

DOM.$win.ready(() => {
  const $accordion = $('.js-logo-wall-accordion');
  if(!$accordion[0])  return;

  const options = {
    el: $accordion,
    changeDuration: 0.4
  };

  $accordion.each((i, el) => {
  
    const $el = $(el)
    options.el = $el;
       /* eslint-disable-next-line */ 
    const $accord = new Accordion(options);
  })

});
