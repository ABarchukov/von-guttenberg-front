import { DOM } from './helpers/_consts';

DOM.$win.ready(() => {
  const $sections = $('.js-projects-section-wrapper');
  $sections.each((index, section) => {
    const $moreBtn = $(section).find('.js-projects-more-btn');
    const $newsHiddenItems = $(section).find('.js-projects-item.is-hidden');
    $moreBtn.on('click', (e) => {
      e.preventDefault();
      $newsHiddenItems.removeClass('is-hidden');
      $moreBtn.remove();
    });
  });
});
