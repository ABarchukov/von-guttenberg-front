import { CLASSES, BREAKPOINTS, DOM } from '../helpers/_consts';
import  '../helpers/_easings';

export default class Dropdown {
  constructor(config) {
    this.containers = $(config.container);
    this.dropdown = config.dropdown;
    this.link = config.link;


    this.handleItemClick = this.handleItemClick.bind(this);

  }

  checkBreakpointChange(){
    if(DOM.$win.innerWidth() > BREAKPOINTS.tablet){
      this.reset()
    }
    // this.checkCssHover()
  }

  checkCssHover(){
    if(DOM.$win.innerWidth() > BREAKPOINTS.tablet){
      this.containers.addClass('is-css-hover')
    }else{
      this.containers.removeClass('is-css-hover')
    }
  }

  checkOpenMenu(){
    if(DOM.$win.innerWidth() > BREAKPOINTS.tablet) return;
    this.containers.each((i, el) => {
      const $el = $(el);
      if($el.hasClass('is-active-link')) {
        $el.addClass(CLASSES.active);
        const $dropdown = $el.find(this.dropdown);
        $dropdown.slideDown(0, 'easeInOutCubic')
      }
    })

  }

  reset() {
    $(this.dropdown).each((index, item) => $(item).removeAttr('style'));
    this.containers.removeClass(CLASSES.active);
  }

  handleItemClick(e) {
    if(DOM.$win.innerWidth() > BREAKPOINTS.tablet) return;

    const $container = $(e.currentTarget).parents('.js-dropdown-container');
    const $dropdown = $container.find(this.dropdown);

    $container.toggleClass(CLASSES.active);

    if ($container.hasClass(CLASSES.active)) {
      // console.log($dropdown[0].scrollHeight, parseInt($dropdown.css('top'), 10))
      // $dropdown.height($dropdown[0].scrollHeight + parseInt($dropdown.css('top'), 10));
      $dropdown.slideDown(300, 'easeInOutCubic')
    } else {
      $dropdown.slideUp(300, 'easeInOutCubic')
      // $dropdown.height(0);
    }
  }

  destroy() {
    [...this.containers].forEach(item => {
      $(item).find(this.link).off('click', this.handleItemClick)
    });
  }

  init() {
    this.checkOpenMenu();
    [...this.containers].forEach(item => {
      $(item).find(this.link).on('click', this.handleItemClick)
    });

  }
}
