import { throttle, debounce } from 'throttle-debounce';
import Swiper from 'swiper/dist/js/swiper';
import { TweenMax, Power0 } from "gsap";
import isTouch from '../helpers/_detectTouch';
import { DOM, BREAKPOINTS, CLASSES, GLOBAL } from '../helpers/_consts';

const minBarDuration = 0.3;
const maxBarDuration = 1.1;
const touch = isTouch();
export default class Scroller{
  constructor (options){
    this.$wrapper = options.wrapper;
   
    this.$slider = this.$wrapper.find('.js-scroller-slider');
    this.$bar = this.$wrapper.find('.js-scroller-bar');
    this.$dropBtn = this.$wrapper.find('.js-scroller-btn');
    this.$progressBar = this.$wrapper.find('.js-scroller-bar-progress');
    this.slides = this.$wrapper.find('.swiper-slide')
    this.slideCounts = this.slides.length;
    // this.FACTOR = (this.slideCounts*0.2)*0.4
    this.prevX = 0;
    this.minBarDuration = 0.3;
    this.maxBarDuration = 1.1;
    this.isDropOpen = false;
    this.isAutoPlay = this.$wrapper.attr('data-auto-play') === 'true';
    this.autoPlayInterval = 0;


    this.init();
    
  }
  
  init(){
    if(!this.$wrapper[0]) return;

    this.calcProgressData();
    this.setSliderWidth();
    
    this.swiper = new Swiper(this.$slider, {
      slidesPerView: 'auto',
      freeMode: true,
      resistanceRatio: 0,
      speed: 400,
    });
    this.checkWidthState()
    this.setEvents();
  }

  initAutoPlay(){
    if(GLOBAL.browser === 'ie' || this.isAutoPlay === false || this.isAutoPlayInited === true ) return;
    this.isAutoPlayInited = true;
    const self = this;

    const fps = 50;
    let now;
    let then = Date.now();
    const interval = 1000/fps;
    let delta;

    function stepAnimation(){
      if(self.isAutoPlay === false) return;

      const trs = self.swiper.getTranslate()

      // const pos = Math.ceil(trs-0.8)
      // console.log(pos)
      now = Date.now();
      delta = now - then;
      if (delta > interval) {
   
         
        then = now - (delta % interval);
         
        self.swiper.setTranslate(trs - 1);
      }
      
     
      if(self.swiper.progress < 1 ){
        requestAnimationFrame(stepAnimation)

       }
    }

    this.autoPlayInterval = setInterval(() => {
      const result = this.checkBlockInViewPort();

      if(result === true){
        clearInterval(this.autoPlayInterval)
        stepAnimation();
      }

    }, 50) 
  }

  stopAutoPlay(){
    this.isAutoPlayInited = true;
    this.isAutoPlay = false
    clearInterval(this.autoPlayInterval)
  }


  checkBlockInViewPort(){
    const elementTop = this.$wrapper.offset().top;
    const elementBottom = elementTop + this.$wrapper.outerHeight();
    const viewportTop = DOM.$win.scrollTop();
    const viewportBottom = viewportTop + DOM.$win.height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  }

  checkWidthState(){
    let slidesWidth = 0;
    this.slides.each((i, el) =>{
      const w = $(el).innerWidth();
      slidesWidth += w;
    })
    
    if(slidesWidth >= this.sliderW) {
      this.$wrapper.addClass(CLASSES.scrolled)
      this.initAutoPlay()
    }else{
      this.$wrapper.removeClass(CLASSES.scrolled)
    }
  }

  calcProgressData(){
    const barW = this.$bar.innerWidth();
    const progressW = this.$progressBar.innerWidth();
    this.barW = barW - progressW;
  }
  
  progressHandle(){
    let x = this.barW * this.swiper.progress;
    x = x < 0 ? 0 : x;
    x = x > this.barW ? this.barW : x;
    let duration = (Math.abs(this.prevX - x)* 0.1);

    duration = duration < minBarDuration ? this.minBarDuration : duration;
    duration = duration > maxBarDuration ? this.maxBarDuration : duration;
    this.prevX = x;
    TweenMax
      .to(this.$progressBar, duration, {x, overwrite: 5, ease: Power0.easeNone})
  }
  
  setSliderWidth(){
    this.sliderW = DOM.$win.innerWidth() - this.$slider.offset().left + 2;
    // this.$slider.css({'width': `${this.sliderW}px`})
  }

  setEvents(){
    this.swiper.on('progress', throttle(15, this.progressHandle.bind(this)));
    this.swiper.once('touchStart', () => { this.isAutoPlay = false });

 

    if(touch){
      DOM.$win.on('orientationchange', debounce(300, () => {this.resizeHandle()}))
    }else{
      DOM.$win.resize(throttle(300, () => {this.resizeHandle()}))
    }
 
    if(this.$dropBtn[0]) this.$dropBtn.on('click', this.dropClickHandle.bind(this))
   
  }

  dropClickHandle(e){
    e.preventDefault();
    this.isAutoPlay = false;
    const $dropWrapper = $(e.currentTarget).parents('.js-scroller-drop-wrapper');

    if($dropWrapper.hasClass(CLASSES.active)){
      this.isDropOpen = true;
      $dropWrapper.removeClass(CLASSES.active);
    }else{
      this.isDropOpen = false;
      $dropWrapper.addClass(CLASSES.active);
    }
  }

  resizeHandle(){
    this.stopAutoPlay()
 
    if(DOM.$win.innerWidth() > BREAKPOINTS.mobile){
      this.setSliderWidth();
      this.checkWidthState();
    }else{
      this.$slider.removeAttr('style');
      
      this.sliderW = DOM.$win.innerWidth();
      this.checkWidthState();
    }

  }
    
}