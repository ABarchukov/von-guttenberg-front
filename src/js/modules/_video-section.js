import Plyr from 'plyr';

export default () => {
  const videoWrappers = document.querySelectorAll('.js-video-wrapper');

  videoWrappers.forEach((videoWrapper) => {
    const video = videoWrapper.querySelector('.js-video');
    const playBtn = videoWrapper.querySelector('.js-video-play-btn');
    const isAutoplay = videoWrapper.getAttribute('data-autoplay') === 'true';

    if(isAutoplay) {
      videoWrapper.classList.add('is-play')
      return
    }
    if(!video || !playBtn) return;


    const options = {
      autoplay: false,
      volume: 0.5,
      muted: false,
      clickToPlay: true,
      hideControls: false,
      ratio: '16:9',
      // loop: true
    }

    // if(isAutoplay) {
    //   options = {
    //     autoplay: true,
    //     volume: 0,
    //     muted: true,
    //     clickToPlay: false,
    //     hideControls: true,
    //     ratio: '16:9',
    //     loop: {
    //       active: true
    //     }
    //   }
    // }

    const player = new Plyr(video, options);

    // if(isAutoplay) {
    //   player.on('ready', () => {
    //     player.muted = true;
    //     console.log(`ready`)
    //     player.play();
    //   });
    // }

    player.on('play', (event) => {
      console.log(`play section`)
      videoWrapper.classList.add('is-play')
    });
    player.on('pause', (event) => {
      console.log(`pause section`)
      videoWrapper.classList.remove('is-play')
    });

    // videoWrapper.addEventListener('click', (e) => {
    //   e.preventDefault()

    //   const isPlayed = videoWrapper.classList.contains('is-play');
    //   console.log(isPlayed)
    //   if(isPlayed) {
    //     player.pause()
    //   } else {
    //     player.play()
    //   }
    // })

  })

}
