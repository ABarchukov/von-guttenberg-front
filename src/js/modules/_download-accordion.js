import { debounce } from 'throttle-debounce';

import scrollFunc from '../helpers/_scrollToFunc';
import { CLASSES, IS_PHONE, DOM } from '../helpers/_consts';

const accordElemClass = 'js-accordion';
const accordContentClass = 'js-accordion-content';
const accordContentWrapperClass = 'js-accordion-content-wrapper';

  const getContentHeight = ($accord) => {
    const $currentContentWrapper = $accord.find(`.${accordContentWrapperClass}`);
    const contentHeight = $currentContentWrapper.innerHeight();
    return contentHeight
  }

  export const setContentHeight = ($accord) => {
    const $currentContent = $accord.find(`.${accordContentClass}`);
    const h = getContentHeight($accord);
    $currentContent[0].style.setProperty('--h', `${h}px`);
  }

  // const resetContentHeight = ($accord) => {
  //   const $currentContent = $accord.find(`.${accordContentClass}`);
  //   $currentContent.removeAttr('style');
  // }

export class Accordion{
  constructor(options){
    this.$wrapper = options.el;
    this.layers = options.layers;
    this.$elems = this.$wrapper.find(`.${accordElemClass}`);
    this.$contents = this.$wrapper.find(`.${accordContentClass}`);
    this.isOpen = false;
    this.btns = this.$wrapper.find('.js-accordion-btn');
    this.canChange = true;
    // this.changeDuration = 400;
    // this.options = {
    //   duration: this.changeDuration,
    //   easing: 'easeInOutCubic', 
    //   complete: this.changeCallback.bind(this)
    // };
    // this.optionsSmall = {
    //   duration: this.changeDuration,
    //   easing: 'easeInOutCubic'
    // };

    this.init();
  }

  init(){
    this.detectOpenElem();
    this.setEvents();

    if(IS_PHONE) this.closeDefaultOpened();
  }

  detectOpenElem(){
    this.$elems.each((i, el) => {

      const $el = $(el);

      setContentHeight($el);

      if($el.hasClass(CLASSES.active)){
        this.$activeElem = $el;
        this.activeIndex = $el.index();
      
      }
    })
  }

  resetPrevLayout() {
    setTimeout(() => {

      this.$elems.each((i, el) => {

        const $el = $(el);

        setContentHeight($el);

        if(!$el.hasClass(CLASSES.active)){
         this.layers[i].reset();
        }
      })
    }, 400)
  }

  closeDefaultOpened() {
    this.$elems.removeClass(CLASSES.active);
    this.btns.find('.controls').removeClass('is-rotate');
    this.resetPrevLayout();
    this.$elems.addClass('is-load');
  }

  clickHandle(e){
    if(this.canChange === false) return;
    this.canChange = false;

    const $btn = $(e.currentTarget);
    const $accord = $btn.parents(`.${accordElemClass}`);
    // const $currentContent = $accord.find(`.${accordContentClass}`);
    const index = $accord.index();
    // const contentHeight = getContentHeight($accord);
    if(this.activeIndex === index){
      
      const isClosing = $accord.hasClass(CLASSES.active)

      if(isClosing) {
        $btn.find('.controls').removeClass('is-rotate')
        $accord.removeClass(CLASSES.active);
        this.resetPrevLayout();
       
      } else {
        $btn.find('.controls').addClass('is-rotate')
        $accord.addClass(CLASSES.active);
        this.layers[index].iso.layout();
      }
      this.canChange = true;

    }else{
      this.btns.find('.controls').removeClass('is-rotate');
      this.$elems.removeClass(CLASSES.active);
      this.resetPrevLayout();

      $btn.find('.controls').addClass('is-rotate');
      $accord.addClass(CLASSES.active);
   
      this.activeIndex = index;

        setTimeout(() => {
          scrollFunc($accord, 0);
          this.canChange = true;
        }, 400)
    }
  
  }

  resizeHandle(){
    this.$elems.each((i, el) => {
      const $el = $(el);
      setContentHeight($el);
    })
  }


  setEvents(){
    this.btns.on('click', this.clickHandle.bind(this));

    if(IS_PHONE){
      DOM.$win.on('orientationchange', debounce(300, () => {this.resizeHandle()}))
    }

  }
  // 
}